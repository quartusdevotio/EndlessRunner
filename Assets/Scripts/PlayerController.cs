using EndlessRunner.Collectible;
using System;
using UnityEngine;

namespace EndlessRunner.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Rigidbody rigidBody;

        [SerializeField] private Vector3 moveSpeed;
        [SerializeField] private Vector3 horizontalMovement;
        [SerializeField] private float[] LanePosition;
        [SerializeField] private float positionTolerance = 0.1f;
        [SerializeField] private int currentLane;

        private Vector3 originSpawn;

        private bool active = false;

        private float screenWidth = Screen.width;

        public Action HitCoin;
        public Action HitObstacle;

        private void Start()
        {
            originSpawn = transform.position;
        }

        private void Update()
        {
            if (!active) return;
            if (Input.GetKeyDown(KeyCode.LeftArrow) && currentLane > 0)
            {
                currentLane -= 1;
            }
            else if( Input.GetKeyDown(KeyCode.RightArrow) && currentLane < LanePosition.Length-1)
            {
                currentLane += 1;
            }

            if(Input.GetMouseButtonDown(0))
            {
                if (Input.mousePosition.x < screenWidth / 2f && currentLane > 0)
                {
                    currentLane -= 1;
                }
                else if (Input.mousePosition.x > screenWidth / 2f && currentLane < LanePosition.Length - 1)
                {
                    currentLane += 1;
                }
            }

            transform.position += moveSpeed * Time.deltaTime;

            if (transform.position.x < LanePosition[currentLane])
            {
                transform.position += horizontalMovement * Time.deltaTime;
            }
            else if (transform.position.x > LanePosition[currentLane])
            {
                transform.position -= horizontalMovement * Time.deltaTime;
            }

            if (Mathf.Abs(transform.position.x - LanePosition[currentLane]) < positionTolerance)
            {
                transform.position = new Vector3(LanePosition[currentLane], transform.position.y, transform.position.z);
            }
        }

        public void ResetState()
        {
            transform.position = originSpawn;
            currentLane = 1;
        }

        public void Activate()
        {
            active = true;
        }

        public void Deactivate()
        {
            active = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("Collectible"))
            {
                CollectibleController collectibleController = other.GetComponent<CollectibleController>();
                CollectibleType collectibleType = collectibleController.collectibleType;
                collectibleController.HitPlayer();
                if(collectibleType == CollectibleType.Coin)
                {
                    HitCoin?.Invoke();
                }
                else
                {
                    HitObstacle?.Invoke();
                }
            }
        }
    }
}