using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner
{
    public class GameCamera : MonoBehaviour
    {
        [SerializeField] private Transform followedObject;
        [SerializeField] private Vector3 offset;

        void Update()
        {
            transform.position = new Vector3(offset.x, offset.y, followedObject.position.z + offset.z);
        }
    }
}
