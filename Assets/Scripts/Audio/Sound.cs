using System;
using UnityEngine;

namespace EndlessRunner.Audio
{
    [Serializable]
    public class Sound
    {
        public string name;
        public AudioClip clip;
    }
}