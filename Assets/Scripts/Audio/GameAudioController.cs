using EndlessRunner.Manager;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner.Audio
{
    public class GameAudioController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private Sound[] soundClips;
        [SerializeField] private AudioSource soundSource;

        private void Start()
        {
            gameManager.HitCoin += OnHitCoin;
            gameManager.GameOver += OnGameOver;
            gameManager.Paused += OnPaused;
        }

        private void OnDestroy()
        {
            gameManager.HitCoin -= OnHitCoin;
            gameManager.GameOver -= OnGameOver;
            gameManager.Paused -= OnPaused;
        }

        private void OnHitCoin()
        {
            PlaySound("HitCoin");
        }

        private void OnGameOver()
        {
            PlaySound("GameOver");
        }

        private void OnPaused()
        {
            PlaySound("Pause");
        }

        private void PlaySound(string name)
        {
            Sound sound = Array.Find(soundClips, x => x.name == name);

            if (sound != null)
            {
                soundSource.clip = sound.clip;
                soundSource.PlayOneShot(soundSource.clip);
            }
        }
    }
}
