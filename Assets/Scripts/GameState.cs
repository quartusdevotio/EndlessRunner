namespace EndlessRunner
{
    public enum GameState
    {
        None,
        Playing,
        Paused,
        GameOver,
    };
}
