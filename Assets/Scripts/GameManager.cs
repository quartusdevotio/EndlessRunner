using EndlessRunner.Player;
using EndlessRunner.Terrain;
using EndlessRunner.UI;
using System;
using UnityEngine;

namespace EndlessRunner.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private StartMenuController startMenuController;
        [SerializeField] private PausePopupController pausePopup;
        [SerializeField] private GameOverPopupController gameOverPopup;
        [SerializeField] private PlayerController playerController;
        [SerializeField] private TerrainPoolController terrainPoolController;

        public event Action<int> HealthChanged;
        public event Action<int> CoinChanged;
        public event Action<float> DistanceChanged;
        public event Action GameOver;
        public event Action Paused;
        public event Action HitCoin;

        [SerializeField] private Camera mainCamera;
        private int coinScore = 0;
        private float distance = 0f;
        private int health = 0;
        public const int MaxHealth = 3;

        [SerializeField] private GameState gameState = GameState.None;

        private void Start()
        {
            playerController.HitCoin += OnHitCoin;
            playerController.HitObstacle += OnHitObstacle;
            gameOverPopup.PlayAgainPressed += OnRestartPressed;
            gameOverPopup.QuitPressed += OnQuitPressed;
            pausePopup.ContinuePressed += Pause;
            pausePopup.RestartPressed += OnRestartPressed;
            pausePopup.QuitPressed += OnQuitPressed;
            startMenuController.PlayPressed += OnPlayPressed;
            startMenuController.QuitPressed += OnQuitPressed;

            health = MaxHealth;
        }

        private void OnDestroy()
        {
            playerController.HitCoin -= OnHitCoin;
            playerController.HitObstacle -= OnHitObstacle;
            gameOverPopup.PlayAgainPressed -= OnRestartPressed;
            gameOverPopup.QuitPressed -= OnQuitPressed;
            pausePopup.ContinuePressed -= Pause;
            pausePopup.RestartPressed -= OnRestartPressed;
            pausePopup.QuitPressed -= OnQuitPressed;
            startMenuController.PlayPressed -= OnPlayPressed;
            startMenuController.QuitPressed -= OnQuitPressed;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && (gameState == GameState.Playing || gameState == GameState.Paused))
            {
                Pause();
            }

            UpdateDistance();
        }

        private void UpdateDistance()
        {
            distance = playerController.transform.position.z;
            DistanceChanged?.Invoke(distance);
        }

        private void OnHitCoin()
        {
            SetCoin(coinScore + 1);
            HitCoin?.Invoke();
        }

        private void OnHitObstacle()
        {
            SetHealth(health - 1);
        }

        private void OnPlayPressed()
        {
            playerController.Activate();

            gameState = GameState.Playing;
        }

        private void OnRestartPressed()
        {
            gameState = GameState.Playing;
            playerController.ResetState();
            playerController.Activate();
            terrainPoolController.ResetPool();
            SetHealth(MaxHealth);
            SetCoin(0);
        }

        private void Pause()
        {
            if (gameState == GameState.Paused)
            {
                gameState = GameState.Playing;
                playerController.Activate();
                pausePopup.Hide();
            }
            else
            {
                Paused?.Invoke();
                gameState = GameState.Paused;
                playerController.Deactivate();
                pausePopup.Show();
            }
        }

        private void OnQuitPressed()
        {
            Application.Quit();
        }

        private void SetHealth(int value)
        {
            health = value;
            GameOverCheck();
            HealthChanged?.Invoke(health);
        }

        private void SetCoin(int value)
        {
            coinScore = value;
            CoinChanged?.Invoke(coinScore);
        }

        private void GameOverCheck()
        {
            if (health <= 0)
            {
                playerController.Deactivate();
                gameState = GameState.GameOver;
                gameOverPopup.Show();
                GameOver?.Invoke();
            }
        }
    }
}