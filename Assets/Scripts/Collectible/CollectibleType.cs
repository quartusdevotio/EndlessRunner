namespace EndlessRunner.Collectible
{
    public enum CollectibleType
    {
        None,
        Coin,
        Obstacle,
    };
}
