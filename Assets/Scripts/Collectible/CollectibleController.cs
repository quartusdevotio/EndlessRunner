using UnityEngine;

namespace EndlessRunner.Collectible
{
    public class CollectibleController : MonoBehaviour
    {
        public CollectibleType collectibleType;

        public void HitPlayer()
        {
            gameObject.SetActive(false);
        }
    }
}
