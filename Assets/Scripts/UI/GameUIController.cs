using EndlessRunner.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace EndlessRunner.UI
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private TMP_Text coinText;
        [SerializeField] private TMP_Text distanceText;
        [SerializeField] private Image[] healthIndicators;

        private void Start()
        {
            gameManager.HealthChanged += OnHealthChanged;
            gameManager.CoinChanged += OnCoinChanged;
            gameManager.DistanceChanged += OnDistanceChanged;
        }

        private void OnDestroy()
        {
            gameManager.HealthChanged -= OnHealthChanged;
            gameManager.CoinChanged -= OnCoinChanged;
            gameManager.DistanceChanged -= OnDistanceChanged;
        }

        private void OnHealthChanged(int health)
        {
            if (health == healthIndicators.Length)
            {
                for(int i=0; i < healthIndicators.Length; i++)
                {
                    healthIndicators[i].enabled = true;
                }
            }
            else
            {
                healthIndicators[health].enabled = false;
            }
        }

        private void OnCoinChanged(int coin)
        {
            coinText.text = "Coin: " + coin.ToString();
        }

        private void OnDistanceChanged(float distance)
        {
            distanceText.text = "Distance: " + distance.ToString("0");
        }
    }
}
