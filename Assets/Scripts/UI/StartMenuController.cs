using System;
using UnityEngine;

namespace EndlessRunner.UI
{
    public class StartMenuController : PopupController
    {
        public event Action PlayPressed;
        public event Action QuitPressed;

        [SerializeField] private Button playButton;
        [SerializeField] private Button quitButton;

        void Start()
        {
            playButton.ButtonUp += OnPlayButtonUp;
            quitButton.ButtonUp += OnQuitButtonUp;
        }

        private void OnDestroy()
        {
            playButton.ButtonUp -= OnPlayButtonUp;
            quitButton.ButtonUp -= OnQuitButtonUp;
        }

        private void OnPlayButtonUp()
        {
            Hide();
            PlayPressed?.Invoke();
        }

        private void OnQuitButtonUp()
        {
            QuitPressed?.Invoke();
        }
    }
}