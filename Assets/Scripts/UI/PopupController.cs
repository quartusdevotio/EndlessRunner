using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner.UI
{
    public class PopupController : MonoBehaviour
    {
        [SerializeField] protected Canvas canvas;

        public void Hide()
        {
            canvas.enabled = false;
        }

        public void Show()
        {
            canvas.enabled = true;
        }

    }
}
