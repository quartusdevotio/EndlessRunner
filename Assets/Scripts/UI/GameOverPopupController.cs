using System;
using UnityEngine;

namespace EndlessRunner.UI
{
    public class GameOverPopupController : PopupController
    {
        public event Action PlayAgainPressed;
        public event Action QuitPressed;

        [SerializeField] private Button playAgainButton;
        [SerializeField] private Button quitButton;

        void Start()
        {
            playAgainButton.ButtonUp += OnRestartButtonUp;
            quitButton.ButtonUp += OnQuitButtonUp;
        }

        void OnDestroy()
        {
            playAgainButton.ButtonUp -= OnRestartButtonUp;
            quitButton.ButtonUp -= OnQuitButtonUp;
        }

        public void OnRestartButtonUp()
        {
            Hide();
            PlayAgainPressed?.Invoke();
        }

        public void OnQuitButtonUp()
        {
            QuitPressed?.Invoke();
        }

    }
}