﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace EndlessRunner.Terrain
{
    public class TerrainPieceController : MonoBehaviour
    {
        public float terrainSize { get; private set; } = 6f;

        [SerializeField] private GameObject[] coins;
        [SerializeField] private GameObject[] obstacles;
        [SerializeField] private GameObject[] benches;
        [SerializeField] private float coinPercentage;
        [SerializeField] private float obstaclePercentage;
        [SerializeField] private float benchPercentage;

        public event Action PlayerExitTerrain;

        public void RandomizeContent()
        {
            int randomCoin = Random.Range(0, coins.Length);
            int randomObstacle = Random.Range(0, obstacles.Length);
            int randomBench = Random.Range(0, benches.Length);

            bool spawnCoin = RandomSpawnChance(coinPercentage);
            bool spawnObstacle = RandomSpawnChance(obstaclePercentage);
            bool spawnBench = RandomSpawnChance(benchPercentage);

            if (spawnCoin)
            {
                coins[randomCoin].SetActive(true);
            }

            if(spawnObstacle && spawnCoin)
            {
                while (randomObstacle == randomCoin)
                {
                    randomObstacle = Random.Range(0, 3);
                }
                obstacles[randomObstacle].SetActive(true);
            }
            else if(spawnObstacle)
            {
                randomObstacle = Random.Range(0, 3);
                obstacles[randomObstacle].SetActive(true);
            }
            
            if(spawnBench)
            {
                benches[randomBench].SetActive(true);
            }
        }

        public void HideContent()
        {
            for(int i=0; i<coins.Length; i++)
            {
                coins[i].SetActive(false);
                obstacles[i].SetActive(false);
            }
            for(int i=0; i<benches.Length; i++)
            {
                benches[i].SetActive(false);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if(other.CompareTag("Player"))
            {
                HideContent();
                gameObject.SetActive(false);
                PlayerExitTerrain?.Invoke();
            }
        }

        private bool RandomSpawnChance(float percentage)
        {
            float random = Random.Range(0f, 1f);
            float total = 100f;

            if (percentage / total >= random)
            {
                return true;
            }
            return false;
        }

    }
}
