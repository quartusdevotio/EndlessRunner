using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner.Terrain
{
    public class TerrainPoolController : MonoBehaviour
    {
        [SerializeField] private List<TerrainPieceController> terrainPieces;
        [SerializeField] private TerrainPieceController terrainToPool;
        [SerializeField] private int amountToPool;
        [SerializeField] private int amountToPreSpawn;

        [SerializeField] private Vector3 spawnPosition;

        [SerializeField] private Transform environmentParent;

        private void Start()
        {
            terrainPieces = new List<TerrainPieceController>();
            for(int i=0; i<amountToPool; i++)
            {
                TerrainPieceController terrain = Instantiate(terrainToPool, environmentParent);
                terrain.gameObject.SetActive(false);
                terrainPieces.Add(terrain);
                terrain.PlayerExitTerrain += OnPlayerExitTerrain;
            }
            PreSpawn();
        }

        private void OnDestroy()
        {
            foreach(TerrainPieceController terrain in terrainPieces)
            {
                terrain.PlayerExitTerrain -= OnPlayerExitTerrain;
                //Destroy(terrain.gameObject);
            }
            terrainPieces.Clear();
            //Destroy di luar loop
        }

        public TerrainPieceController GetPooledTerrain()
        {
            for(int i=0; i<terrainPieces.Count; i++)
            {
                if(!terrainPieces[i].gameObject.activeInHierarchy)
                {
                    return terrainPieces[i];
                }
            }
            return null;
        }

        public void ResetPool()
        {
            spawnPosition = new Vector3(0f, 0f, -terrainToPool.terrainSize);
            foreach (TerrainPieceController terrain in terrainPieces)
            {
                terrain.gameObject.SetActive(false);
                terrain.HideContent();
            }
            PreSpawn();
        }

        private void OnPlayerExitTerrain()
        {
            SpawnTerrain();
        }

        private void SpawnTerrain()
        {
            TerrainPieceController terrain = GetPooledTerrain();
            if (terrain != null)
            {
                terrain.transform.position = spawnPosition + new Vector3(0f, 0f, terrain.terrainSize);
                spawnPosition = terrain.transform.position;
                terrain.gameObject.SetActive(true);
                terrain.RandomizeContent();
            }
        }

        private void PreSpawn()
        {
            for (int i = 0; i < amountToPreSpawn; i++)
            {
                SpawnTerrain();
                if(i == 0)
                {
                    terrainPieces[i].HideContent();
                }
            }
        }
    }
}